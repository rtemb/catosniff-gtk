package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/google/jsonapi"
	"github.com/gotk3/gotk3/gtk"
)

// UI of admin app
type UI struct {
	Builder         *gtk.Builder
	questionField   *gtk.Entry
	submitBtn       *gtk.Button
	answerContainer *gtk.Grid
	questOpen       *gtk.RadioButton
	questClosed     *gtk.RadioButton
	status          *gtk.TextView
}

// CreateBuilder create gtk builder
func (u *UI) CreateBuilder() {
	// Инициализируем GTK.
	gtk.Init(nil)

	var err error
	u.Builder, err = gtk.BuilderNew()
	if err != nil {
		log.Fatal(err)
	}
}

// LoadAndRunUI Load UI from main.glade file and display all widgets
func (u *UI) LoadAndRunUI() {
	if err := u.Builder.AddFromFile("main.glade"); err != nil {
		log.Fatal(err)
	}
	mainWindow, err := u.Builder.GetObject("window_main")
	if err != nil {
		log.Fatal(err)
	}

	win := mainWindow.(*gtk.Window)
	win.Connect("destroy", func() {
		gtk.MainQuit()
	})

	win.Present()
	win.ShowAll()
}

// CollectControls collect UI controls for easy access to them
func (u *UI) CollectControls() {
	// Get question text field
	obj, err := u.Builder.GetObject("question_text")
	if err != nil {
		log.Fatal(err)
	}
	u.questionField = obj.(*gtk.Entry)

	// get Submit button
	obj2, err2 := u.Builder.GetObject("submit")
	if err2 != nil {
		log.Fatal(err2)
	}
	u.submitBtn = obj2.(*gtk.Button)

	// get Answer container
	obj3, err3 := u.Builder.GetObject("answer_container")
	if err3 != nil {
		log.Fatal(err3)
	}
	u.answerContainer = obj3.(*gtk.Grid)

	// get Answer container
	obj4, err4 := u.Builder.GetObject("q_open")
	if err4 != nil {
		log.Fatal(err4)
	}
	u.questOpen = obj4.(*gtk.RadioButton)

	// get Answer container
	obj5, err5 := u.Builder.GetObject("q_closed")
	if err5 != nil {
		log.Fatal(err5)
	}
	u.questClosed = obj5.(*gtk.RadioButton)

	// get Status field
	obj6, err6 := u.Builder.GetObject("status")
	if err6 != nil {
		log.Fatal(err6)
	}
	u.status = obj6.(*gtk.TextView)
}

// BindHandlers bind handler func to UI
func (u *UI) BindHandlers() {
	u.questClosed.Connect("clicked", func() {
		u.answerContainer.Show()
	})

	u.questOpen.Connect("clicked", func() {
		u.answerContainer.Hide()
	})

	u.submitBtn.Connect("clicked", func() {
		questText, isOpen, err := u.collectQuestion()
		if err != nil {
			log.Panic(err)
		}

		if len(questText) == 0 {
			u.showMsg("Question is empty")
			return
		}

		answers, err5 := u.collectAnswers()
		if err5 != nil {
			log.Panic(err5)
		}

		if isOpen == false && len(answers) == 0 {
			log.Println("For closed questions need to add answer(s)")
		}

		question := Question{
			Text:         questText,
			IsOpen:       isOpen,
			AnswVariants: answers,
		}

		status, err := question.send()
		u.showMsg(fmt.Sprintf("HttpCode: %v. Err: %s", status, err))
	})
}

func (u *UI) collectAnswers() ([]*AnswVariant, error) {
	var answers []*AnswVariant
	for i := 1; i <= 4; i++ {
		textField, err := u.answerContainer.GetChildAt(0, i)
		if err != nil {
			return nil, err
		}

		text, err2 := textField.GetProperty("text")
		if err2 != nil {
			return nil, err
		}

		if len(text.(string)) > 0 {
			answ := new(AnswVariant)
			answ.ID = i - 1
			answ.Text = text.(string)
			answers = append(answers, answ)
		}
	}

	return answers, nil
}

func (u *UI) collectQuestion() (string, bool, error) {
	questText, err := u.questionField.GetProperty("text")
	if err != nil {
		return "", false, err
	}

	questType, err2 := u.questOpen.GetProperty("active")
	if err2 != nil {
		return "", false, err2
	}

	return questText.(string), questType.(bool), nil
}

func (q *Question) send() (httpCode int, err error) {
	requestBody := bytes.NewBuffer(nil)
	err2 := jsonapi.MarshalPayload(requestBody, q)
	if err2 != nil {
		fmt.Fprintln(os.Stderr, err2)
		return 0, err2
	}

	req, err := http.NewRequest(http.MethodPost, "http://localhost:8081/question", requestBody)
	req.Header.Set("Accept", jsonapi.MediaType)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return 0, err
	}

	defer resp.Body.Close()

	return resp.StatusCode, err
}

func (u *UI) showMsg(msg string) {
	buffer, err := u.status.GetBuffer()
	if err != nil {
		log.Fatal("Unable to get buffer:", err)
	}

	buffer.SetText(msg)
	u.status.Show()
}

// AnswVariant variant of answer on question
type AnswVariant struct {
	ID   int    `jsonapi:"primary,answVariants"`
	Text string `jsonapi:"attr,text"`
}

func (a *AnswVariant) String() string {
	return string(a.ID) + a.Text
}

// Question strict
type Question struct {
	ID           int            `jsonapi:"primary,questions"`
	Text         string         `jsonapi:"attr,text"`
	IsOpen       bool           `jsonapi:"attr,isOpen"`
	AnswVariants []*AnswVariant `jsonapi:"relation,answVariants"`
}
