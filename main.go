package main

import "github.com/gotk3/gotk3/gtk"

func main() {
	ui := UI{}
	ui.CreateBuilder()
	ui.LoadAndRunUI()
	ui.CollectControls()
	ui.BindHandlers()
	gtk.Main()
}
